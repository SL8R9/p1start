package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;


public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{

	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
		private Node<E> nextNode;
		private Node<E> previousNode;
		
		public CircularSortedDoublyLinkedListIterator() {
			this.nextNode = (Node<E>) header.getNext();
			this.previousNode = (Node<E>) header.getPrevious();
		}
		@Override
		public boolean hasNext() {
			return nextNode != null;
		}
		
		public boolean hasPrevious(){
			return previousNode != null;
		}
		
		public E previous(){
			if (this.hasPrevious()){
				E result = this.previousNode.getElement();
				this.previousNode = this.previousNode.getPrevious();
				return result;
			}
			else{
				throw new NoSuchElementException();
			}
		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
		
	}
	
	
	private static class Node<E> {
		private E element;
		private Node<E> next;
		private Node<E> previous;
		
		public Node() {
			super();
		}
		
		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		
		public Node<E> getPrevious(){
			return previous;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		
		public void setPrevious(Node<E> previous){
			this.previous = previous;
		}
	}
	
	private Node<E> header;
	private int currentSize;
	private CarComparator<E> comparator;
	private Comparator<E> cmp;
	
	public CircularSortedDoublyLinkedList(CarComparator<E> Cmp) {
		this.header = new Node<>();
		this.currentSize = 0;
		header.setNext(header);
		header.setPrevious(header);
		this.comparator = Cmp;
	}

	public CircularSortedDoublyLinkedList(Comparator<E> intComp) {
		// TODO Auto-generated constructor stub
		this.header = new Node<>();
		this.currentSize = 0;
		header.setNext(header);
		header.setPrevious(header);
		this.cmp = intComp;
	}

	@Override
	public Iterator<E> iterator() {
		return new CircularSortedDoublyLinkedListIterator<E>();
	}

	@Override
	public boolean add(E obj) {
		
		Node<E> newNode = new Node<>();
		Node<E> temp = null;
		newNode.setElement(obj);
		this.currentSize++;
		
		//add method for sorted list tester
		
//		for (temp = this.header.getNext(); temp != this.header; temp = temp.getNext()){
//			if (this.cmp.compare(temp.getElement(),  obj) >=0){
//				newNode.setNext(temp);
//				newNode.setPrevious(temp.getPrevious());
//				temp.getPrevious().setNext(newNode);
//				temp.setPrevious(newNode);
//				return true;
//			}
//		}
//		newNode.setNext(this.header);
//		newNode.setPrevious(this.header.getPrevious());
//		this.header.getPrevious().setNext(newNode);
//		this.header.setPrevious(newNode);
//		return true;
//	}
		
		//add method for cars
		for (temp = this.header.getNext(); temp != this.header; temp = temp.getNext()){
			if (this.comparator.compare((Car)temp.getElement(), (Car) obj) >=0){
				newNode.setNext(temp);
				newNode.setPrevious(temp.getPrevious());
				temp.getPrevious().setNext(newNode);
				temp.setPrevious(newNode);
				return true;
			}
		}
		newNode.setNext(this.header);
		newNode.setPrevious(this.header.getPrevious());
		this.header.getPrevious().setNext(newNode);
		this.header.setPrevious(newNode);
		return true;
	}

	

	@Override
	public int size() {
		return currentSize;
	}

	@Override
	public boolean remove(E obj) {
		int i = this.lastIndex(obj);
		if (i < 0) {
			return false;
		}else {
			this.remove(i);
			return true;
		}
	}

	@Override
	public boolean remove(int index) {
		if ((index < 0) || (index >= this.currentSize)){
			throw new IndexOutOfBoundsException();
		}
		else {
			Node<E> temp = this.header;
			int currentPosition =0;
			Node<E> target = null;
			
			while (currentPosition != index) {
				temp = temp.getNext();
				currentPosition++;
			}
			target = temp.getNext();
			temp.setNext(target.getNext());
			target.getNext().setPrevious(temp);
			target.setElement(null);
			target.setNext(null);
			target.setPrevious(null);
			this.currentSize--;
			return true;			
		}
	}

	@Override
	public int removeAll(E obj) {
		int count = 0;
		while (this.remove(obj)) {
			count++;
		}
		return count;
	}

	@Override
	public E first() {
		return (this.isEmpty() ? null : this.header.getNext().getElement());
	}

	@Override
	public E last() {
		return this.header.getPrevious().getElement();
	}

	@Override
	public E get(int index) {
		if (index < 0 || index >= this.currentSize){
			throw new IndexOutOfBoundsException();
		}
		Node<E> temp  = this.getPosition(index);
		return temp.getElement();		
	}
	
	private Node<E> getPosition(int index){
		int currentPosition=0;
		Node<E> temp = this.header.getNext();
		
		while(currentPosition != index) {
			temp = temp.getNext();
			currentPosition++;
		}
		return temp;
	}

	@Override
	public void clear() {
		while (!this.isEmpty()){
			this.remove(0);
		}
	}

	@Override
	public boolean contains(E e) {
		return this.lastIndex(e) >=0;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public int firstIndex(E e) {
		int i = 0;
		for (Node<E> temp = this.header.getNext(); temp != this.header; 
				temp = temp.getNext(), i++) {
			if (temp.getElement().equals(e)) {
				return i;
			}
		}
		// not found
		return -1;
	}

	@Override
	public int lastIndex(E e) {
		int i = this.currentSize-1;
		for (Node<E> temp = this.header.getPrevious(); temp != this.header; 
				temp = temp.getPrevious(), i--) {
			if (temp.getElement().equals(e)) {
				return i;
			}
		}
		// not found
		return -1;
	}

	
}
