package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path ("/cars") 

public class CarManager {
	 private final CircularSortedDoublyLinkedList<Car> carList = CarList.getInstance();   
	 
	 
	 @GET
	 @Produces (MediaType.APPLICATION_JSON)
	 public Car[] getCars() {
		 
		 Car[] cars = new Car[carList.size()];
		 for (int i=0; i< carList.size(); i++) {
			 cars[i] = carList.get(i);
		 }
		 return cars;
	 }
	 
	 @GET
	 @Path("/{id}")
	 @Produces (MediaType.APPLICATION_JSON)
	 public Car getCar(@PathParam("id") long id) {
		 for (int i=0; i<carList.size(); i++) {
			 if (carList.get(i).getCarId() == id){
				 return carList.get(i);
			 }
		 }
		 
		 throw new NotFoundException();
	 }
	 
	 @POST
	 @Path("/add")
	 @Produces (MediaType.APPLICATION_JSON)
	 public Response addCar(Car newCar) {
		 for (int i=0; i<carList.size();i++) {
			 if (carList.get(i).getCarId() == newCar.getCarId()) {
				 return Response.status(403).build();
			 }
		 }
		 
		 carList.add(newCar);
		 return Response.status(201).build();
	 }
	 
	 
	 @PUT
	 @Path("{id}/update")
	 @Produces (MediaType.APPLICATION_JSON)
	 public Response updateCar(Car carUpdate) {
		 for (Car car : carList) {
			 if (car.getCarId() == carUpdate.getCarId()) {
				 carList.remove(car);
				 carList.add(carUpdate);
				 return Response.status(Response.Status.OK).build();
			 }
		 }
		 return Response.status(Response.Status.NOT_FOUND).build();
	 }
	 
	 @DELETE
	 @Path("/{id}/delete")
	 @Produces (MediaType.APPLICATION_JSON)
	 public Response deleteCar(@PathParam("id") long id) {
		 boolean found = false;
		 for(int i=0; i<carList.size(); i++) {
			 if (carList.get(i).getCarId() == id) {
				 carList.remove(i);
				 found = true;
				 return Response.status(Response.Status.OK).build();
			 }
		 }
		 if (!found) {
			 throw new NotFoundException();
		 }
		 return Response.status(Response.Status.NOT_FOUND).build();
	 }
	  
}
