package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator<E> implements Comparator<Car> {

	public int compare(Car c1, Car c2) {

		String car1 = c1.getCarBrand() + c1.getCarModel() + c1.getCarModelOption();
		String car2 = c2.getCarBrand() + c2.getCarModel() + c2.getCarModelOption();
		
		return car1.compareTo(car2);
	}

	
}
